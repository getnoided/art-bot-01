import praw
import config
import time
import requests
import json
replied_list = []


def bot_login():
    r = praw.Reddit(username=config.username,
        password=config.password,
        client_id=config.client_id,
        client_secret = config.client_secret,
        # user agent is just something reddit uses to identify the bot. 
        user_agent="art bot v1")
    print "Successfully logged in"
    return r

"""Returns art token""" 
def getToken():
    r = requests.post('https://api.artsy.net/api/tokens/xapp_token?client_id=&client_secret=')
    artsyResponse = r.json()
    token = artsyResponse['token']
    print 'Got token'     
    return token
    

def getArt(artsyToken):
    print "getting Art"
    url = 'https://api.artsy.net/api/artworks?&sample=1'
    headers = {'X-Xapp-Token': artsyToken}
    response = requests.get(url, headers=headers)
    artObj = response.json()
    return artObj


def prepareArtBody(artObj):
    print(artObj)
    a_str = """Here you go:
       ###{title}
       {date}   
        ![{title}]({image})
       {description}""".format(title=artObj['title'], date=artObj['date'],description=artObj['blurb'],image=artObj['_links']['thumbnail']['href'])
    print a_str
    return a_str
    print "Body= " + body

def run_bot(r,artsyToken):
    print "Looking for comments"
    for comment in  r.subreddit('test').comments(limit=25):
        if "ill take one art please" in comment.body:
            print "Suitable content found"+ comment.id
            artObj = getArt(artsyToken)
            commentResponse = prepareArtBody(artObj)
            comment.reply(commentResponse)


# r = bot_login()
#while True:
 #   run_bot(r)

# token = getToken()
# artObj = getArt(token)
# prepareArtBody(artObj)


## Production
r = bot_login()
token = getToken()
run_bot(r,token)